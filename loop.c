#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <sys/time.h>
#include <assert.h>

#include "libavl.h"
#include "libdebug.h"

#include "./struct.h"
#include "./element/struct.h"
#include "./element/finder.h"

void guirouter_loop(struct guirouter* this)
{
	ENTER;
	Display* display = this->display;
	struct timeval then, now, dt;
	gettimeofday(&then, NULL);
	gettimeofday(&now, NULL);
	XEvent event;
	while(this->all_elements.n)
	{
		while(XPending(display))
		{
			XNextEvent(display, &event);
			verpv(event.type);
			struct avl_node* node = 
				avl_tree_search(
					&(this->all_elements),
					guirouter_element_finder,
					&(event.xany.window));
			assert(node);
			struct guirouter_element* element = node->data;
			verpv(element);
			switch(event.type)
			{
				case ButtonPress:
				{
					if(element->callbacks->on_button_pressed)
					{
						element->callbacks->on_button_pressed(
							element, &event);
					}
					break;
				};
				case ButtonRelease:
				{
					if(element->callbacks->on_button_released)
					{
						element->callbacks->on_button_released(
							element, &event);
					}
					break;
				};
				case Expose:
				{
					if(element->callbacks->on_expose_event)
					{
						element->callbacks->on_expose_event(
							element, &event);
					}
					break;
				};
				case KeyPress:
				{
					if(element->callbacks->on_key_pressed)
					{
						element->callbacks->on_key_pressed(
							element, &event);
					}
					break;
				};
				case KeyRelease:
				{
					if(element->callbacks->on_key_released)
					{
						element->callbacks->on_key_released(
							element, &event);
					}
					break;
				};
				case MotionNotify:
				{
					if(element->callbacks->on_motion_event)
					{
						element->callbacks->on_motion_event(
							element, &event);
					}
					break;
				};
				case ConfigureNotify:
				{
					if(element->callbacks->on_configure_event)
					{
						element->callbacks->on_configure_event(
							element, &event);
					}
					break;
				};
				case ClientMessage: TODO;
			}
		}
	}
	EXIT;
}

#if 0

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdbool.h>
#include <unistd.h>

#include "libdebug.h"

#include "./struct.h"
#include "./redraw.h"

#include "./states/struct.h"

void window_loop(struct window* this)
{
	ENTER;
	for(bool keep_going = true;keep_going;then = now)
	{
		while(XPending(display))
		{
			struct state* current = this->states.last->data;
			XNextEvent(display, &event);
			verpv(event.type);
			switch(event.type)
			{
				case ButtonPress:
				{
					current->callbacks->on_button_pressed(
						current, &event.xbutton);
					break;
				}
				case ButtonRelease:
				{
					current->callbacks->on_button_released(
						current, &event.xbutton);
					break;
				}
				case Expose:
				{
					window_redraw(this);f
					break;
				}
				#if 0
				case KeyPress:
				{
					current->callbacks->on_key_pressed(
						current, &event.xkey);
					break;
				}
				case KeyRelease:
				{
					current->callbacks->on_key_released(
						current, &event.xkey);
					break;
				}
				#endif
				case MotionNotify:
				{
					current->callbacks->on_mouse_motion(
						current, &event.xmotion);
					break;
				}
				case ConfigureNotify:
				{
					XConfigureEvent* spef = &event;
					this->size.x = spef->width;
					this->size.y = spef->height;
					break;
				}
				case ClientMessage: {
					XClientMessageEvent* event_spef = &(event.xclient);
					if((Atom) event_spef->data.l[0] == wm_delete_window) {
						keep_going = false;
					}
					break;
				}
				default: break;
			}
		}
		struct state* current = this->states.last->data;
		gettimeofday(&now, NULL);
		timersub(&now, &then, &dt);
		if(current->callbacks->on_timeout)
		{
			current->callbacks->on_timeout(current,
				(double) dt.tv_sec + (double) dt.tv_usec / (1000 * 1000)
			);
		}
		usleep(10 * 1000);
	}
	EXIT;
}





#endif
