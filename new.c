#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>

#include "libdebug.h"
#include "libavl.h"
#include "libarray.h"

#include "./struct.h"
#include "./element/compare.h"

struct guirouter* new_guirouter(Display* display)
{
	ENTER;
	struct guirouter* this = malloc(sizeof(struct guirouter));
	this->display = display;
	avl_init_tree(&(this->all_elements), guirouter_element_compare, NULL);
	this->wm_delete_window = XInternAtom(display, "WM_DELETE_WINDOW", False);
	EXIT;
	return this;
}

