#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>

#include "libdebug.h"

#include "./struct.h"

int guirouter_element_finder(struct guirouter_element* ele, Window* b)
{
	return ele->window - *b;
}

