#include <stdio.h>
#include <stdlib.h>

#include "libdebug.h"

#include "./struct.h"

struct guirouter_element* new_guirouter_element(
	int window,
	struct guirouter_callbacks* callbacks,
	int size)
{
	ENTER;
	struct guirouter_element* this = malloc(size);
	this->window = window;
	this->callbacks = callbacks;
	EXIT;
	return this;
}

