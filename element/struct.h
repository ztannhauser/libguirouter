
#include <X11/Xlib.h>

struct guirouter_callbacks
{
	void (*on_motion_event)(struct guirouter_widget* this, XMotionEvent* event);
	void (*on_button_pressed)(struct guirouter_widget* this, XButtonEvent* event);
	void (*on_button_released)(struct guirouter_widget* this, XButtonEvent* event);
	void (*on_key_pressed)(struct guirouter_widget* this, XMotionEvent* event);
	void (*on_key_released)(struct guirouter_widget* this, XMotionEvent* event);
	void (*on_configure_event)(struct guirouter_widget* this, XConfigureEvent* event);
	void (*on_expose_event)(struct guirouter_widget* this, XExposeEvent* event);
	void (*on_timeout)(struct guirouter_widget* this, double delta);
	void (*on_close_signal)(struct guirouter_widget* this);
};

struct guirouter_element
{
	Window window;
	struct guirouter_callbacks* callbacks;
};

