#include <stdio.h>
#include <stdlib.h>

#include "libdebug.h"
#include "libavl.h"

#include "./defines.h"
#include "./struct.h"
#include "./element/struct.h"

void guirouter_push(struct guirouter* this, struct guirouter_element* new)
{
	ENTER;
	Window window = new->window;
	XSelectInput(this->display, window, WINDOW_EVENT_MASK);
	XSetWMProtocols(this->display, window, &(this->wm_delete_window), 1);
	avl_insert(&(this->all_elements), new);
	EXIT;
}

