
#ifndef LIBGUIROUTER_H
#define LIBGUIROUTER_H

#include <X11/Xlib.h>

struct guirouter_element;

struct guirouter_callbacks
{
	void (*on_motion_event)(struct guirouter_widget* this, XMotionEvent* event);
	void (*on_button_pressed)(struct guirouter_widget* this, XButtonEvent* event);
	void (*on_button_released)(struct guirouter_widget* this, XButtonEvent* event);
	void (*on_key_pressed)(struct guirouter_widget* this, XMotionEvent* event);
	void (*on_key_released)(struct guirouter_widget* this, XMotionEvent* event);
	void (*on_configure_event)(struct guirouter_widget* this, XConfigureEvent* event);
	void (*on_expose_event)(struct guirouter_widget* this, XExposeEvent* event);
	void (*on_timeout)(struct guirouter_widget* this, double delta);
	void (*on_close_signal)(struct guirouter_widget* this);
};


#define as_element ((struct guirouter_element*) this)

struct guirouter_element
{
	struct guirouter_callbacks* callbacks;
	Window window;
};


struct guirouter
{
	Display* display;
	Atom wm_delete_window;
	struct avl_tree all_elements;
};

struct guirouter* new_guirouter(Display* display);

struct guirouter_element* new_guirouter_element(
	int window,
	struct guirouter_callbacks* callbacks,
	int size);

void guirouter_push(struct guirouter* this, struct guirouter_element* new);

void guirouter_loop(struct guirouter* this);

#endif
